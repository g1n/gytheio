use std::fs;
use std::env;
use std::path::Path;
use std::io::prelude::*;
use std::io::ErrorKind;
use std::net::TcpListener;
use std::net::TcpStream;
use std::process::Command;

extern crate argparse;
use argparse::{ArgumentParser, StoreTrue, Store};


fn main() {
    let mut verbose = false;
    let mut host = "localhost".to_string();
    let mut port = 3000; // TODO: change to 300
    let mut root_dir = "/var/spartan/".to_string();
    // Arguments
    {  
        let mut ap = ArgumentParser::new();
        ap.set_description("Experimental spartan server");
        ap.refer(&mut verbose)
            .add_option(&["-v", "--verbose"], StoreTrue,
            "Be verbose");
        ap.refer(&mut host)
            .add_option(&["--host"], Store,
            "Hostname");
        ap.refer(&mut port)
            .add_option(&["-p", "--port"], Store,
            "Port");
        ap.refer(&mut root_dir)
            .add_option(&["-d", "--root-dir"], Store,
            "Root directory");
        ap.parse_args_or_exit();
    }
    if verbose {
        println!("Work in progress");
    }
    println!("Starting gytheio on {}:{}", host, port);
    let listener = TcpListener::bind(format!("{}:{}", host, port)).unwrap();

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        handle_connection(stream, root_dir.to_string());
    }
}

fn handle_connection(mut stream: TcpStream, root_dir: String) {
    let mut buffer = [0; 1024];

    stream.read(&mut buffer).unwrap();
    println!("{}", String::from_utf8_lossy(&buffer[..]));
    // let _echo = b"$hostname /echo 0\r\n"; // TODO: fix echo behaviour

    let request =  String::from_utf8_lossy(&buffer[..]);
    let parsed_request: Vec<&str> = request.split(' ').collect();
    let  _post: Vec<&str> = request.split('\n').collect();
    if parsed_request.len() != 3 {
        let status_line = "5 Request error";
        send_response(status_line, "".to_string(), stream);
        return;
    }

    let _post = _post[1].to_string();
    let _host = parsed_request[0];
    let _path = parsed_request[1];
    let _post_len = parsed_request[2];
    
    let status_line = if Path::new(&format!("{}{}", root_dir, _path).to_string()).exists() && !_path.contains(".."){
        "2 text/gemini"
    } else {
        "4 Not found"
    };
     
    let mut _contents = String::new();
    let full_path = format!("{}{}", root_dir, _path);
    let mut exists = Path::new(&format!("{}{}", root_dir, _path).to_string()).exists();
    if exists {
        if Path::new(&full_path).is_file() {
            _contents = fs::read_to_string(format!("{}{}", root_dir, _path)).unwrap();
            if _path.contains(".sh") {
                env::set_var("QUERY_STRING", _post);
                let command = Command::new(format!("{}", full_path)).output().unwrap();
                _contents = String::from_utf8(command.stdout).unwrap();
            }
        } else {
            exists = Path::new(&format!("{}{}/index.gmi", root_dir, _path).to_string()).exists();
            if exists { 
                if _path.contains("..") {
                    let status_line = "4 Not Found";
                    send_response(status_line, "".to_string(), stream); 
                    return;
                }
                _contents = fs::read_to_string(format!("{}{}index.gmi", root_dir, _path)).unwrap_or_else(|error| {
                    if error.kind() == ErrorKind::NotFound {
                        panic!("NotFound error!");
                    } else {
                        panic!("Other error!");
                    }
                });
            } else {
                if _path.contains("..") {
                    let status_line = "4 Not Found";
                    send_response(status_line, "".to_string(), stream); 
                    return;
                }
                _contents = format!("=> ..\n");
                let path_to_folder = full_path.clone();
                let files = fs::read_dir(path_to_folder).unwrap();
                for file in files {
                    let path_to_folder = full_path.clone();
                    let path = file.unwrap().path();
                    let file = path.strip_prefix(path_to_folder);
                    let file = file.unwrap().display();
                    if path.is_dir() {
                        _contents += &format!("=> {}/\n", file).to_string(); 
                    } else {
                        _contents += &format!("=> {}\n", file).to_string();
                    }
                }
            }
        }
    }
    
    send_response(status_line, _contents, stream); 
}

fn send_response(status_line: &str, contents: String, mut stream: TcpStream) {
    let response = format!(
        "{}\r\n{}",
        status_line,
        contents
    );

    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
    
}
